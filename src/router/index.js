import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import TableDedail from "../views/TableDetail.vue";
import TrackingPO from "../components/TrackingPO/TrackingPO.vue";
import POcomplate from "../components/POcomplate/POcomplate.vue";
import Cropper from "../views/PageCrop.vue";
import PageDetailPO from "../views/PageDetailPO.vue";
import PagePresetName from "../views/PagePresetName.vue";
import PageProfile from "../views/PageProfile.vue";
import PageSetPreset from "../views/PageSetPreset.vue";
import PagePrintReport from "../views/PagePrintReport.vue";
import PageAuthen from "../views/PageAuthen.vue";
import PageSetPresetStepper from "../views/PageSetPresetStepper.vue";
import PageSetPresetStepperTab from "../views/PageSetPresetStepperTab.vue";
import PageSetPresetSteppers from "../views/PageSetPresetSteppers.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    // path: "/authen/:idcard/:token",
    path: "/authen",
    name: "PageAuthen",
    component: PageAuthen,
  },
  {
    path: "/print/report",
    name: "PagePrintReport",
    component: PagePrintReport,
  },
  {
    path: "/cropper",
    name: "Cropper",
    component: Cropper,
  },
  {
    path: "/presetname",
    name: "PagePresetName",
    component: PagePresetName,
  },
  {
    path: "/setpreset",
    name: "PageSetPreset",
    component: PageSetPreset,
  },
  {
    path: "/preset/stepper",
    name: "PageSetPresetStepper",
    component: PageSetPresetStepper,
  },
  {
    path: "/preset/steppers",
    name: "PageSetPresetSteppers",
    component: PageSetPresetSteppers,
  },
  {
    path: "/preset/stepper/tab",
    name: "PageSetPresetStepperTab",
    component: PageSetPresetStepperTab,
  },
  {
    path: "/profile",
    name: "PageProfile",
    component: PageProfile,
  },
  {
    path: "/detailPO",
    name: "PageDetailPO",
    component: PageDetailPO,
  },
  {
    path: "/main",
    name: "TableDedail",
    component: TableDedail,
    props: true,
    children: [
      {
        path: "TrackingPO",
        name: "TrackingPO",
        component: TrackingPO,
      },
      {
        path: "POcomplate",
        name: "POcomplate",
        component: POcomplate,
      },
      // {
      //   path: "/preset/stepper",
      //   name: "PageSetPresetStepper",
      //   component: PageSetPresetStepper,
      // },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
